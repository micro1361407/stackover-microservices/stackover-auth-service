# StackOver - Auth-Service

## Настройка профилей для разработки и подключения к БД

1. Перед запуском приложения необходимо обновить свои настройки БД и liquibase в application.properties
2. Необходимо внести изменения в конфигурацию запуска приложения:

![](src/main/resources/auth-desc/config-02-01.png)

![](src/main/resources/auth-desc/config-02-02.png)

Если таких полей у вас нет, то необходимо нажать «Modify options» и там добавить эти поля.

![](src/main/resources/auth-desc/config-02-03.png)

Для environment variables имена и значения переменных указываются в формате _КЛЮЧ1=значение;КЛЮЧ2=значение_

- **Тип БД:** DB_BASE=postgresql;
- **Сервер:** DB_SERVER=localhost;
- **Порт:** DB_PORT=5432;
- **Имя БД:** DB_NAME=stackover_profile_service;

Имена и пароли для профилей dev и local:

DB_USERNAME=postgres;
DB_PASSWORD=root;

Значения полей могут различаться.

В зависимости от того, какой профиль вам необходим, меняем значение перед запуском – `dev` или `local` для VM options.
Например:

`-ea -Dspring.profiles.active=local`

DB_BASE=jdbc:postgresql;DB_SERVER=localhost;DB_PORT=5432;DB_NAME=stackover_auth_service;DB_USERNAME=postgres;DB_PASSWORD=root

`-ea -Dspring.profiles.active=dev`

DB_BASE=jdbc:postgresql;DB_SERVER=localhost;DB_PORT=5435;DB_NAME=stackover_auth_service;DB_USERNAME=postgres;DB_PASSWORD=root