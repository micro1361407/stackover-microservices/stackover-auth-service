-- liquibase formatted sql

-- changeset Torchez:1713784270094-1
CREATE SEQUENCE IF NOT EXISTS account_generator START WITH 11799 INCREMENT BY 1;

-- changeset Torchez:1713784270094-2
CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START WITH 1 INCREMENT BY 1;

-- changeset Torchez:1713784270094-3
CREATE TABLE account
(
    id         BIGINT      NOT NULL,
    email      VARCHAR(255),
    password   VARCHAR(60) NOT NULL,
    enabled    BOOLEAN     NOT NULL,
    locale_tag VARCHAR(255),
    role_id    BIGINT,
    CONSTRAINT pk_account PRIMARY KEY (id)
);

-- changeset Torchez:1713784270094-4
CREATE TABLE refresh_token
(
    id          BIGINT NOT NULL,
    account_id  BIGINT,
    token       VARCHAR(255),
    expiry_date TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT pk_refreshtoken PRIMARY KEY (id)
);

-- changeset Torchez:1713784270094-5
CREATE TABLE role
(
    id   BIGINT      NOT NULL,
    name VARCHAR(20) NOT NULL,
    CONSTRAINT pk_role PRIMARY KEY (id)
);

-- changeset Torchez:1713784270094-6
ALTER TABLE account
    ADD CONSTRAINT FK_ACCOUNT_ON_ROLE FOREIGN KEY (role_id) REFERENCES role (id);

-- changeset Torchez:1713784270094-7
ALTER TABLE refresh_token
    ADD CONSTRAINT FK_REFRESHTOKEN_ON_ACCOUNT FOREIGN KEY (account_id) REFERENCES account (id);

