package stackover.auth.service.converters;

import org.springframework.stereotype.Component;
import stackover.auth.service.dto.account.request.SignupRequestDto;
import stackover.auth.service.dto.profile.ProfilePostDto;

@Component
public class SignUpDtoProfilePostDtoConverter {

    public ProfilePostDto convertToProfilePostDto(SignupRequestDto signupRequestDto, Long accountId) {
        return new ProfilePostDto(
                accountId,
                signupRequestDto.email(),
                signupRequestDto.fullName(),
                signupRequestDto.city(),
                signupRequestDto.persistDateTime(),
                signupRequestDto.linkSite(),
                signupRequestDto.linkGitHub(),
                signupRequestDto.linkVk(),
                signupRequestDto.about(),
                signupRequestDto.imageLink(),
                signupRequestDto.nickname());
    }

}
