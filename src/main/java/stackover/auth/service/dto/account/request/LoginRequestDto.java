package stackover.auth.service.dto.account.request;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;

@Schema(description = "DTO с запросом данных аутентификации пользователя")
public record LoginRequestDto(

        @Schema(description = "Запрос e-mail пользователя", example = "user@example.com")
        @NotBlank String email,

        @Schema(description = "Запрос пароля пользователя", example = "12345678")
        @NotBlank String password) {
}
