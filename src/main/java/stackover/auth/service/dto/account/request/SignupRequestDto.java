package stackover.auth.service.dto.account.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Schema(description = "DTO с запросом регистрационных данных пользователя")
public record SignupRequestDto(

        @Schema(description = "Пароль пользователя", example = "12345678")
        @NotBlank String password,

        @Schema(description = "Роль пользователя", example = "USER")
        @NotBlank String roleName,

        @Schema(description = "E-mail пользователя", example = "userS@example.com")
        @NotBlank String email,

        @Schema(description = "Полное имя пользователя", example = "John Doe")
        String fullName,

        @Schema(description = "Город проживания пользователя", example = "Moscow")
        String city,
        @Schema(description = "Дата регистрации", example = "01.01.2000")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
        LocalDateTime persistDateTime,
        @Schema(description = "Ссылка на сайт")
        String linkSite,
        @Schema(description = "Ссылка на профиль GitHub")
        String linkGitHub,
        @Schema(description = "Ссылка на профиль ВКонтакте")
        String linkVk,
        @Schema(description = "О себе")
        String about,
        @Schema(description = "Аватар профиля пользователя")
        String imageLink,
        @Schema(description = "Никнейм")
        String nickname) {
}
