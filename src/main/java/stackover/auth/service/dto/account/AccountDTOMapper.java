package stackover.auth.service.dto.account;

import org.springframework.stereotype.Component;
import stackover.auth.service.dto.account.response.AccountResponseDto;
import stackover.auth.service.model.Account;
@Component
public class AccountDTOMapper {

    public AccountResponseDto toDto(Account account) {
        return AccountResponseDto.builder()
            .id(account.getId())
            .email(account.getEmail())
            .role(account.getRole().getName())
            .enabled(account.getEnabled())
            .build();
    }
}
