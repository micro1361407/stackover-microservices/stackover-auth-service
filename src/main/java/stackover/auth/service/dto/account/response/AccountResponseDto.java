package stackover.auth.service.dto.account.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import stackover.auth.service.util.enums.RoleNameEnum;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Builder
@Schema(description = "Ответ на запрос данных об аутентификации и авторизации пользователя")
public record AccountResponseDto (
        @Schema(description = "ID аккаунта пользователя")
        long id,
        @Schema(description = "E-mail пользователя", example = "userS@example.com")
        @NotEmpty
        @Email String email,
        @Schema(description = "Права пользователя", example = "USER")
        @NotEmpty RoleNameEnum role,
        @Schema(description = "Состояние активности профиля", example = "true")
        boolean enabled) {
}
