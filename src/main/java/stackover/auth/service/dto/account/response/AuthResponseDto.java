package stackover.auth.service.dto.account.response;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;

@Schema(description = "Аутентификация пользователя с возвратом JWT-токена")
public record AuthResponseDto(
        @Schema(description = "ID аккаунта пользователя")
        @NotBlank Long accountId,
        @Schema(description = "E-mail пользователя", example = "userS@example.com")
        @NotBlank String email,
        @Schema(description = "JWT-токен")
        String accessToken,
        @Schema(description = "Срок действия выданного токена (в секундах)")
        Long expiresIn) {
}
