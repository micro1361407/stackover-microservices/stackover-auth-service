package stackover.auth.service.dto.profile;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Schema(description = "Запрос на создание профиля")
public record ProfilePostDto(
        @Schema(description = "ID аккаунта пользователя")
        @NotNull Long accountId,

        @Schema(description = "E-mail пользователя", example = "userS@example.com")
        @NotBlank @Email String email,

        @Schema(description = "Полное имя пользователя", example = "John Doe")
        @NotBlank String fullName,

        @Schema(description = "Город проживания пользователя", example = "Moscow")
        String city,

        @Schema(description = "Дата регистрации", example = "01.01.2000")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
        LocalDateTime persistDateTime,

        @Schema(description = "Ссылка на сайт")
        String linkSite,
        @Schema(description = "Ссылка на профиль GitHub")
        String linkGitHub,
        @Schema(description = "Ссылка на профиль ВКонтакте")
        String linkVk,
        @Schema(description = "О себе")
        String about,
        @Schema(description = "Аватар профиля пользователя")
        String imageLink,
        @Schema(description = "Никнейм")
        @NotBlank String nickname) {

        public ProfilePostDto(String email, String fullName, String city,
                              LocalDateTime persistDateTime, String linkSite, String linkGitHub,
                              String linkVk, String about, String imageLink, String nickname) {
                this(null, email, fullName, city, persistDateTime, linkSite, linkGitHub, linkVk, about, imageLink, nickname);
        }
}
