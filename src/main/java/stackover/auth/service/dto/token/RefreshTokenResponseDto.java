package stackover.auth.service.dto.token;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

@Builder
@Schema(name = "RefreshTokenResponseDto", description = "Обновление RefreshToken")
public record RefreshTokenResponseDto(
        @Schema(description = "Access-токен")
        String accessToken,
        @Schema(description = "Refresh-токен")
        String refreshToken,
        @Schema(description = "Токен протух или заблокирован")
        boolean blocked) {
}