package stackover.auth.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.Instant;

/**&#064;Note:  2024-05 Добавлены AllArgs, NoArgsConstructor, конструктор без id, изменен тип генерации id */

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class RefreshToken {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;
    @OneToOne
    private Account account;
    private String token;
    private Instant expiryDate;

    public RefreshToken(Account account, String token, Instant expiryDate) {
        this.account = account;
        this.token = token;
        this.expiryDate = expiryDate;
    }
}
