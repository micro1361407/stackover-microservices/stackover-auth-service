package stackover.auth.service.model;

import lombok.Builder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Builder
public record AuthenticationStatus (@NotNull Boolean isAuthenticated,
                                    @NotEmpty String message) {
}
