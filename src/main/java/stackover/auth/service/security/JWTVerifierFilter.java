package stackover.auth.service.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import stackover.auth.service.util.JWTokensProvider;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class JWTVerifierFilter extends OncePerRequestFilter {

    private final JWTokensProvider jwtTokensProvider;
    private final AccountDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        String bearerToken = request.getHeader("Authorization");
        String authToken = null;

        if (!StringUtils.isEmpty(bearerToken) && bearerToken.startsWith("Bearer ")) {
            authToken = bearerToken.replace("Bearer ", "");
        }

        var authClaim = jwtTokensProvider.getClaims(authToken);

        String email = authClaim.getSubject();

        if (!StringUtils.isEmpty(email) || SecurityContextHolder.getContext().getAuthentication() == null) {

            final UserDetails userDetails = userDetailsService.loadUserByUsername(email);

            if (Boolean.TRUE.equals(jwtTokensProvider.validateToken(authToken, userDetails))) {

                List<GrantedAuthority> grantedAuthorities =
                        Collections.singletonList(new SimpleGrantedAuthority(authClaim.get("role", String.class)));

                var authentication = new UsernamePasswordAuthenticationToken(email, null, grantedAuthorities);
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);

                request.setAttribute("email", email);
                request.setAttribute("role", grantedAuthorities);
            }
        }

        filterChain.doFilter(request, response);
    }
}
