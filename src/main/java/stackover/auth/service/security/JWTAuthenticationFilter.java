package stackover.auth.service.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import stackover.auth.service.dto.account.response.AccountResponseDto;
import stackover.auth.service.model.AuthenticationStatus;
import stackover.auth.service.model.RefreshToken;
import stackover.auth.service.service.AccountService;
import stackover.auth.service.service.RefreshTokenService;
import stackover.auth.service.util.JWTokensProvider;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    private final AuthenticationManager authenticationManager;
    private final JWTokensProvider jwtTokensProvider;
    private final RefreshTokenService refreshTokenService;
    private final AccountService accountService;
    private final ObjectMapper mapper = new ObjectMapper();


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {

        var email = request.getParameter("email");
        var password = request.getParameter("password");

        log.debug("email & password {}, {}", email, password);

        Authentication authentication = new UsernamePasswordAuthenticationToken(email, password);
        return authenticationManager.authenticate(authentication);

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authentication) throws IOException {

        AccountResponseDto accountResponseDto = new AccountResponseDto(
                accountService.findAccountByEmail(authentication.getName()).get().getId(),
                authentication.getName(),
                accountService.findAccountByEmail(authentication.getName()).get().getRole().getName(),
                accountService.findAccountByEmail(authentication.getName()).get().getEnabled());

        log.info("accountResponseDto {}", accountResponseDto.email());

        var refreshToken = jwtTokensProvider.generateRefreshToken(accountResponseDto);

        refreshTokenService.save(new RefreshToken(
                accountService.findAccountByEmail(authentication.getName()).get(),
                refreshToken,
                jwtTokensProvider.getExpirationDate(refreshToken).toInstant()
        ));

        if (StringUtils.isEmpty(request.getHeader(AUTHORIZATION_HEADER))) {
            response.addHeader(AUTHORIZATION_HEADER, "Bearer " + refreshToken);
            log.debug("header {}", request.getHeader(AUTHORIZATION_HEADER));
        }

        response.setHeader(AUTHORIZATION_HEADER, String.format("Bearer %s", refreshToken));

        AuthenticationStatus authenticationStatus = AuthenticationStatus.builder()
                .isAuthenticated(true)
                .message(HttpStatus.ACCEPTED.toString())
                .build();
        response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream().write(mapper.writeValueAsBytes(authenticationStatus));
    }
}
