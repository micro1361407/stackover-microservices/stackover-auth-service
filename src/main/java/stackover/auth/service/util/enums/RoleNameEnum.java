package stackover.auth.service.util.enums;

public enum RoleNameEnum {
    USER,
    ADMIN
}
