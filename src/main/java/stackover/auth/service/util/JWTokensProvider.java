package stackover.auth.service.util;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import stackover.auth.service.dto.account.response.AccountResponseDto;

import javax.crypto.SecretKey;
import javax.validation.constraints.NotNull;
import java.security.Key;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Slf4j
@Service
public class JWTokensProvider {

    private final SecretKey jwtSecretKey;

    public JWTokensProvider(
            @Value("${jwt.secret.key}") String jwtSecretKey
    ) {
        this.jwtSecretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecretKey));
    }

    public String generateAccessToken(@NotNull AccountResponseDto accountResponseDto) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant accessExpiration = now.plusMinutes(15).atZone(ZoneId.systemDefault()).toInstant();
        final Date accessExpirationDate = Date.from(accessExpiration);

        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setSubject(accountResponseDto.email())
                .setExpiration(accessExpirationDate)
                .signWith(jwtSecretKey)
                .claim("accountId", accountResponseDto.id())
                .compact();
    }

    public String generateRefreshToken(@NotNull AccountResponseDto accountResponseDto) {
        final LocalDateTime now = LocalDateTime.now();
        final Instant refreshExpiration = now.plusDays(30).atZone(ZoneId.systemDefault()).toInstant();
        final Date refreshExpirationDate = Date.from(refreshExpiration);

        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setSubject(accountResponseDto.email())
                .setExpiration(refreshExpirationDate)
                .signWith(jwtSecretKey)
                .claim("accountId", accountResponseDto.id())
                .claim("role", accountResponseDto.role().name())
                .claim("enabled", accountResponseDto.enabled())
                .compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {

        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isExpired(token));
    }

    public String extractUsername(String token) {
        return getClaims(token).getSubject();
    }

    public Boolean validateToken(@NotNull String token) {
        try {
            log.debug("Token: {}", token);
            final Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(jwtSecretKey).build().parseClaimsJws(token);
            final Claims claims = claimsJws.getBody();
            log.debug("Claims: {}", claims);
            return true;
        } catch (ExpiredJwtException expiredJwtException) {
            log.error("Expired JWT token: {}", expiredJwtException.getMessage());
        } catch (UnsupportedJwtException unsupportedJwtException) {
            log.error("Unsupported JWT token: {}", unsupportedJwtException.getMessage());
        } catch (MalformedJwtException malformedJwtException) {
            log.error("Invalid JWT token: {}", malformedJwtException.getMessage());
        } catch (SignatureException signatureException) {
            log.error("Invalid JWT signature: {}", signatureException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            log.error("JWT claims string is empty: {}", illegalArgumentException.getMessage());
        } catch (Exception e) {
            log.error("Invalid JWT: {}", e.getMessage());
        }
        return false;
    }

    public Claims getClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(jwtSecretKey)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public Date getExpirationDate(String token) {
        return getClaims(token).getExpiration();
    }

    public boolean isExpired(String token) {
        return getClaims(token).getExpiration().before(new Date());
    }

}