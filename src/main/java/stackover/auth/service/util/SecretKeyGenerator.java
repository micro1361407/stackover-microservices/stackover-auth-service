package stackover.auth.service.util;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SecretKeyGenerator {
    public static void main(String[] args) {

        log.info("Access Key: {}", generateSecretKey());
        log.info("Refresh Key: {}", generateSecretKey());
    }

    public static String generateSecretKey() {
        return Encoders.BASE64.encode(Keys.secretKeyFor(SignatureAlgorithm.HS256).getEncoded());
    }
}
