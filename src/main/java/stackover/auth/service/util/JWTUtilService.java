package stackover.auth.service.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Deprecated
public class JWTUtilService {

    public static final String SECRET_ACCESS_KEY = "SET-SECRET-ACCESS-KEY";
    public static final String SECRET_REFRESH_KEY = "SET-SECRET-REFRESH-KEY";
    private static final Long EXPIRES_IN = (long) 1000 * 60 * 60 * 10;
    private static final Long REFRESH_EXPIRATION = (long) 1000 * 60 * 60 * 10;


    public Jws<Claims> validateToken(final String token) {
        return Jwts.parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token);
    }

    public String generateToken(String email) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, email);
    }

    //TODO Check SecretKey creation and Expiration LifeTime for Token
    private String createToken(Map<String, Object> claims, String email) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(email) //sub
                .setIssuedAt(new Date(System.currentTimeMillis())) //iss
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRES_IN)) //exp
                .signWith(getSigningKey(), SignatureAlgorithm.HS256).compact(); //sign
    }

    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_ACCESS_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    //TODO Test

    public boolean isExpired(String token) {
        return validateToken(token).getBody().getExpiration().before(new Date());
    }

    public String getSubject(String token) {
        return validateToken(token).getBody().getSubject();
    }

    public Long expiresIn(String token) {
        return validateToken(token).getBody().getExpiration().getTime();
    }
}
