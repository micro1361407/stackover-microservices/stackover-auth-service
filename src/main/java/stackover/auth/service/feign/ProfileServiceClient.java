package stackover.auth.service.feign;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import stackover.auth.service.dto.profile.ProfilePostDto;
import stackover.auth.service.exception.ApiRequestException;

@FeignClient(name = "STACKOVER-PROFILE-SERVICE", fallbackFactory = ProfileServiceClient.ProfilesClientFallbackFactory.class,
        path = "/api/inner/profile")
public interface ProfileServiceClient {

    @PostMapping("/create")
    ResponseEntity<HttpStatus> createProfileByPostDto(@RequestBody ProfilePostDto profilePostDto);

    @Component
    class ProfilesClientFallbackFactory implements FallbackFactory<FallbackWithFactory> {
        @Override
        public FallbackWithFactory create(Throwable cause) {
            return new FallbackWithFactory(cause.getMessage());
        }
    }

    record FallbackWithFactory(String reason) implements ProfileServiceClient {

        @Override
        public ResponseEntity<HttpStatus> createProfileByPostDto(ProfilePostDto profilePostDto) {

            String responseMessage = """
                    Something was wrong with account id %s and reason: %s
                    """.formatted(profilePostDto.accountId(), reason);
            throw new ApiRequestException(responseMessage);
        }
    }
}