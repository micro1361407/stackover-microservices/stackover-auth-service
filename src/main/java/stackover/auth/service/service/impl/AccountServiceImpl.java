package stackover.auth.service.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.auth.service.dto.account.request.SignupRequestDto;
import stackover.auth.service.dto.account.response.AccountResponseDto;
import stackover.auth.service.dto.profile.ProfilePostDto;
import stackover.auth.service.feign.ProfileServiceClient;
import stackover.auth.service.model.Account;
import stackover.auth.service.repository.AccountDAORepository;
import stackover.auth.service.repository.AccountDTORepository;
import stackover.auth.service.service.AccountService;
import stackover.auth.service.service.RoleService;
import stackover.auth.service.util.enums.RoleNameEnum;

import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountDTORepository accountDTORepository;
    private final AccountDAORepository accountDAORepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    private final ProfileServiceClient profileServiceClient;

    /**
     * <p>Получаем AccountResponseDto на основании введенных данных логина и пароля. Первое, что происходит в методе, –
     * это сравнение пароля с паролем аккаунта в базе данных, ассоциированного с введенным логином. Если они совпадают,
     * то возвращаем AccountResponseDto. И позже по этим данным происходит аутентификация.</p>
     *
     * <p>Важное здесь – это использование метода passwordEncoder.matches(), который возвращает true, если пароли (raw & encoded) совпадают.</p>
     *
     * @param email    email как поле логина
     * @param password пароль
     * @return Optional AccountResponseDto, если найден и пара логин/пароль соответствует ожиданиям, или UsernameNotFoundException, который отлавливается в методе /login контроллера.
     */

    @Transactional(readOnly = true)
    @Override
    public Optional<AccountResponseDto> getAccountDtoByEmailAndPassword(String email, String password) {

        final Optional<Account> byEmail = accountDAORepository.findByEmail(email);

        if (byEmail.isEmpty() || Boolean.FALSE.equals(passwordEncoder.matches(password, byEmail.get().getPassword()))) {

            throw new UsernameNotFoundException("Incorrect pair email/password");
        }

        var accountResponseDto = accountDTORepository.findByEmail(email);

        if (accountResponseDto.isEmpty()) {

            throw new UsernameNotFoundException("Incorrect email");
        }
        return accountResponseDto;
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<Account> findAccountByEmail(String email) {
        return accountDAORepository.findByEmail(email);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<AccountResponseDto> getAccountDtoByEmail(String email) {
        return accountDTORepository.findByEmail(email);
    }

    @Transactional(readOnly = true)
    @Override
    public Boolean isExist(Long id) {
        return accountDTORepository.isExist(id).isPresent();
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<AccountResponseDto> getAccountDtoById(Long id) {
        return accountDTORepository.findByAccountId(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Boolean checkExistByIdAndRole(Long accountId, RoleNameEnum role) {
        return accountDTORepository.existsAccountsByIdAndRole(accountId, role).isPresent();
    }

    @Transactional
    @Modifying
    @Override
    public void saveAccount(SignupRequestDto signupRequestDto) {

        Account account = Account.builder()
                .email(signupRequestDto.email())
                .password(passwordEncoder.encode(signupRequestDto.password()))
                .localeTag("ru")
                .role(roleService.findRoleByName(RoleNameEnum.valueOf(signupRequestDto.roleName())))
                .build();

        accountDAORepository.save(account);

        var accountId = accountDAORepository.findByEmail(signupRequestDto.email()).get().getId();

        profileServiceClient.createProfileByPostDto(new ProfilePostDto(
                accountId,
                signupRequestDto.email(),
                signupRequestDto.fullName(),
                signupRequestDto.city(),
                signupRequestDto.persistDateTime(),
                signupRequestDto.linkSite(),
                signupRequestDto.linkGitHub(),
                signupRequestDto.linkVk(),
                signupRequestDto.about(),
                signupRequestDto.imageLink(),
                signupRequestDto.nickname()));
    }

    @Transactional(readOnly = true)
    @Override
    public Account findAccountById(Long accountId) {
        return accountDAORepository.findAccountById(accountId);
    }
}
