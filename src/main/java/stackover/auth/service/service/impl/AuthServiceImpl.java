package stackover.auth.service.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.auth.service.converters.SignUpDtoProfilePostDtoConverter;
import stackover.auth.service.dto.account.request.LoginRequestDto;
import stackover.auth.service.dto.account.request.SignupRequestDto;
import stackover.auth.service.dto.account.response.AuthResponseDto;
import stackover.auth.service.model.AuthenticationStatus;
import stackover.auth.service.model.RefreshToken;
import stackover.auth.service.security.AccountDetailsService;
import stackover.auth.service.service.AccountService;
import stackover.auth.service.service.AuthService;
import stackover.auth.service.service.RefreshTokenService;
import stackover.auth.service.util.JWTokensProvider;

@Slf4j
@RequiredArgsConstructor
@Service
public class AuthServiceImpl implements AuthService {

    private final JWTokensProvider jwtTokensProvider;
    private final AccountService accountService;
    private final AccountDetailsService userDetailsService;
    private final RefreshTokenService refreshTokenService;

    @Override
    @Transactional
    public ResponseEntity<String> signUp(SignupRequestDto signupRequestDto) {

        log.error("Sign up request: {}", signupRequestDto);

        if (accountService.findAccountByEmail(signupRequestDto.email()).isPresent()) {

            return ResponseEntity.status(HttpStatus.CONFLICT).body("Account with this email already exists");
        }

        try {
            accountService.saveAccount(signupRequestDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).body("Account created");
    }
    /**
     * <p>Аутентифицируем пользователя по входящим логину и паролю. Сначала проверяем существование
     * нужной нам пары credentials в Базе Данных, а затем производим аутентификацию или выбрасываем
     * UsernameNotFoundException, если такая пара не существует.</p>
     * <p>После прохождения аутентификации создаются новый Access и Refresh токены, Refresh-токен обновляется в БД.</p>
     *
     * @param  loginRequestDto  DTO с данными логина и пароля пользователя
     * @return                 В случае успеха возвращаем AuthResponseDto аутентифицированного пользователя
     *                         с обновленным Access-токеном. После чего устанавливаем этот токен в заголовок.
     */
    @Override
    @Transactional
    public AuthResponseDto login(LoginRequestDto loginRequestDto) {

        AuthenticationStatus status = authenticate(loginRequestDto.email(), loginRequestDto.password());

        if (Boolean.FALSE.equals(status.isAuthenticated())) {
            throw new UsernameNotFoundException("Wrong email or password in first step authentication.");
        }

        var accountDetails = userDetailsService.loadUserByUsername(loginRequestDto.email());
        log.error("Account details: {}", accountDetails.getAuthorities());
        log.error("Account details: {}", accountDetails.getUsername());
        Authentication authentication = new UsernamePasswordAuthenticationToken(accountDetails.getUsername(), null, accountDetails.getAuthorities());

        if (authentication.isAuthenticated()) {

        SecurityContextHolder.getContext().setAuthentication(authentication);

        var accountResponseDto = accountService.getAccountDtoByEmailAndPassword(
                loginRequestDto.email(), loginRequestDto.password()).get();

        var accessToken = jwtTokensProvider.generateAccessToken(accountResponseDto);
        var accessExpiresIn = jwtTokensProvider.getExpirationDate(accessToken).getTime();

        var refreshToken = jwtTokensProvider.generateRefreshToken(accountResponseDto);

        refreshTokenService.save(new RefreshToken(
                accountService.findAccountById(accountResponseDto.id()),
                refreshToken,
                jwtTokensProvider.getExpirationDate(refreshToken).toInstant()));

        return new AuthResponseDto(
                accountResponseDto.id(),
                accountResponseDto.email(),
                accessToken,
                accessExpiresIn);

        } else {
            throw new UsernameNotFoundException("Bad Credentials");
        }
    }

    /**
     * Вспомогательный метод для проверки введенных в LoginRequestDto данных, содержащих логин и пароль.
     *
     * @param  email     Строка email из LoginRequestDto
     * @param  password  Строка password из LoginRequestDto
     * @return           AuthenticationStatus с результатом проверки готовности данных к аутентификации.
     *  Поле isAuthenticated принимает значение true, если пользователь с веденной парой логин/пароль существует, и false,
     *  если нет.
     */
    private AuthenticationStatus authenticate(String email, String password) {
        if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)) {
            throw new UsernameNotFoundException("Incorrect email or password. Second step authentication.");
        }
        return accountService.getAccountDtoByEmailAndPassword(email, password).isPresent() ?
                new AuthenticationStatus(true, "Authentication Successful") :
                new AuthenticationStatus(false, "Authentication Failed");
    }
}
