package stackover.auth.service.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.auth.service.dto.token.RefreshTokenResponseDto;
import stackover.auth.service.exception.ApiRequestException;
import stackover.auth.service.model.RefreshToken;
import stackover.auth.service.repository.RefreshTokenRepository;
import stackover.auth.service.service.AccountService;
import stackover.auth.service.service.RefreshTokenService;
import stackover.auth.service.util.JWTokensProvider;


import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService {

    private final RefreshTokenRepository refreshTokenRepository;
    private final AccountService accountService;
    private final JWTokensProvider jwt;

    /**
     * Если RefreshToken не проходит валидацию, то выбрасывается исключение, если проходит -
     * проверяем связанные с аккаунтом токены, если есть – удаляем и сохраняем новый. Если нет -
     * просто сохраняем новый токен.
     *
     * @param  refreshToken   Сущность RefreshToken для сохранения
     */

    @Transactional
    @Override
    public void save(RefreshToken refreshToken) {

        final Optional<RefreshToken> tokenFromDB = findRefreshTokenByAccountId(refreshToken.getAccount().getId());

        if (Boolean.FALSE.equals(jwt.validateToken(refreshToken.getToken()))) {
            throw new ApiRequestException("Refresh token is not valid");
        } else {
            tokenFromDB.ifPresent(refreshTokenRepository::delete);
            refreshTokenRepository.save(refreshToken);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    @Override
    public Optional<RefreshToken> findRefreshTokenByAccountId(Long accountId) {
        return refreshTokenRepository.findRefreshTokenByAccountId(accountId);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<RefreshToken> findByEmail(String subject) {
        return refreshTokenRepository.findByEmail(subject);
    }
    /**
     * По входяще строке токена валидируем его и, если валидный, возвращаем новый RefreshTokenResponseDto
     * с обновленными Access и Refresh токенами. Предварительно удаляем связанный с аккаунтом Refresh-токен.
     * Если токен невалидный, возвращаем пустый Optional.
     *
     * @param  token  токен для проверки, по результатам которой
     * @return        Optional с new RefreshTokenResponseDto если токен валидный, иначе пустой Optional.empty()ю
     */

    @Transactional
    @Override
    public Optional<RefreshTokenResponseDto> getRefreshToken(String token) {

        if (Boolean.TRUE.equals(jwt.validateToken(token))) {

            var claims = jwt.getClaims(token);
            var accountResponseDto = accountService.getAccountDtoByEmail(claims.getSubject()).get();

            var newAccessToken = jwt.generateAccessToken(accountResponseDto);
            var newRefreshToken = jwt.generateRefreshToken(accountResponseDto);

            refreshTokenRepository.deleteByAccountId(accountResponseDto.id());

            refreshTokenRepository.save(new RefreshToken(
                    accountService.findAccountById(accountResponseDto.id()),
                    newRefreshToken,
                    jwt.getExpirationDate(newRefreshToken).toInstant()));

            return Optional.of(RefreshTokenResponseDto.builder()
                    .accessToken(newAccessToken)
                    .refreshToken(newRefreshToken)
                    .blocked(false)
                    .build());
        }

        return Optional.empty();
    }
}

