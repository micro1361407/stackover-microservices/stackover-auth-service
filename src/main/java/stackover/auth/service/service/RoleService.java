package stackover.auth.service.service;

import stackover.auth.service.model.Role;
import stackover.auth.service.util.enums.RoleNameEnum;

import java.util.List;

public interface RoleService {

    Role findRoleByName(RoleNameEnum name);

    List<Role> findAll();
}
