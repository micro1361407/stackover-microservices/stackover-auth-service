package stackover.auth.service.service;

import stackover.auth.service.dto.token.RefreshTokenResponseDto;
import stackover.auth.service.model.RefreshToken;

import java.util.Optional;

public interface RefreshTokenService {

    void save(RefreshToken refreshToken);

    Optional<RefreshToken>findByToken(String token);

    Optional<RefreshToken> findRefreshTokenByAccountId(Long accountId);

    Optional<RefreshToken> findByEmail(String subject);

    Optional<RefreshTokenResponseDto> getRefreshToken(String token);

}
