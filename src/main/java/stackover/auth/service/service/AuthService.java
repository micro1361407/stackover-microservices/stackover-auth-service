package stackover.auth.service.service;

import org.springframework.http.ResponseEntity;
import stackover.auth.service.dto.account.request.LoginRequestDto;
import stackover.auth.service.dto.account.request.SignupRequestDto;
import stackover.auth.service.dto.account.response.AuthResponseDto;
import stackover.auth.service.dto.token.RefreshTokenResponseDto;

import javax.servlet.http.HttpServletResponse;

public interface AuthService {


    ResponseEntity<String> signUp(SignupRequestDto signupRequestDto);

    AuthResponseDto login(LoginRequestDto loginRequestDto);

}
