package stackover.auth.service.service;

import stackover.auth.service.dto.account.request.SignupRequestDto;
import stackover.auth.service.dto.account.response.AccountResponseDto;
import stackover.auth.service.model.Account;
import stackover.auth.service.util.enums.RoleNameEnum;

import java.util.Optional;

public interface AccountService {

    Optional<AccountResponseDto> getAccountDtoByEmail(String email);

    Optional<AccountResponseDto> getAccountDtoByEmailAndPassword(String email, String password);

    Boolean isExist(Long id);

    Optional<AccountResponseDto> getAccountDtoById(Long id);

    Boolean checkExistByIdAndRole(Long accountId, RoleNameEnum role);

    void saveAccount(SignupRequestDto signupRequestDto);


    Account findAccountById(Long accountId);

    Optional<Account> findAccountByEmail(String email);

}
