package stackover.auth.service.exception;

import java.io.IOException;

public class AccountNotAvailableException extends RuntimeException {
    public AccountNotAvailableException(Exception e) {
        super();
    }

    public AccountNotAvailableException(String message) {
        super(message);
    }
}
