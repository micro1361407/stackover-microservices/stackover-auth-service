package stackover.auth.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stackover.auth.service.dto.account.request.LoginRequestDto;
import stackover.auth.service.dto.account.request.SignupRequestDto;
import stackover.auth.service.dto.account.response.AuthResponseDto;
import stackover.auth.service.dto.token.RefreshTokenResponseDto;
import stackover.auth.service.service.AccountService;
import stackover.auth.service.service.AuthService;
import stackover.auth.service.service.RefreshTokenService;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/auth")
public class AuthRestController {

    private final AuthService authenticationService;
    private final AccountService accountService;
    private final RefreshTokenService refreshService;

    private static final String TOKEN_PREFIX = "Bearer ";

    @PostMapping("/signup")
    public ResponseEntity<String> signUp(@RequestBody SignupRequestDto signupRequestDto) {
        return authenticationService.signUp(signupRequestDto);
    }

    @Operation(summary = "User Login API", description = "Authentication by login/password")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Login Successful", content =
                    {@Content(mediaType = "application/json", schema = @Schema(implementation = AuthResponseDto.class))}),
            @ApiResponse(responseCode = "401", description = "Login Failed", content = @Content),
            @ApiResponse(responseCode = "403", description = "Authorization Field. Access Denied", content = @Content)
    })
    @PostMapping(value = "/login")
    public ResponseEntity<AuthResponseDto> login(@RequestBody LoginRequestDto loginRequestDto) {

        AuthResponseDto authResponseDto = authenticationService.login(loginRequestDto);
        var accountResponseDto = accountService.getAccountDtoByEmail(loginRequestDto.email());

        if (accountResponseDto.isPresent()) {
            var accessToken = authResponseDto.accessToken();
            return ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION, TOKEN_PREFIX + accessToken)
                    .body(authResponseDto);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .header(HttpHeaders.AUTHORIZATION, TOKEN_PREFIX)
                    .body(null);
        }
    }

    @Operation(summary = "Tokens refresh API", description = "Returns new Access & Refresh tokens")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Refresh Successful", content =
                    {@Content(mediaType = "application/json", schema = @Schema(implementation = RefreshTokenResponseDto.class))}),
            @ApiResponse(responseCode = "400", description = "Refresh Failed", content = @Content)
    })
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @PostMapping("/refresh")
    public ResponseEntity<RefreshTokenResponseDto> getRefreshTokenResponseDto(@RequestBody String token) {

        Optional<RefreshTokenResponseDto> refreshTokenResponseDto = refreshService.getRefreshToken(token);

        return refreshTokenResponseDto.map(tokenResponseDto -> ResponseEntity.status(HttpStatus.ACCEPTED)
                .header(HttpHeaders.AUTHORIZATION, TOKEN_PREFIX + tokenResponseDto.accessToken())
                .body(tokenResponseDto)).orElseGet(() -> ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .header(HttpHeaders.AUTHORIZATION, TOKEN_PREFIX).build());
    }
}
