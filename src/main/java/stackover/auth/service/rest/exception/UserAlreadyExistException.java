package stackover.auth.service.rest.exception;

import javax.validation.constraints.NotBlank;

public class UserAlreadyExistException extends RuntimeException {
    public UserAlreadyExistException(@NotBlank String email) {
        super(email);
    }
}
