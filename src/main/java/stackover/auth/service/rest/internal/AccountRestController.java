package stackover.auth.service.rest.internal;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import stackover.auth.service.dto.account.response.AccountResponseDto;
import stackover.auth.service.service.AccountService;
import stackover.auth.service.util.enums.RoleNameEnum;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/internal/account")
public class AccountRestController {

    private final AccountService accountService;

    /**
     * Проверка существования аккаунта по id. Метод был реализован на первом этапе прохождения проекта,
     * сохранен для обратной совместимости с текущими версиями кода, смердженного в GitLab. К удалению.
     *
     * @param  id	Идентификатор аккаунта
     * @return         	Возвращается значение TREE/FALSE
     */

    @Operation(summary = "Проверка существования аккаунта по id. ")
    @ApiResponse(responseCode = "200", description = "TRUE/FALSE. Первая реализация метода. Сохранено для обратной совместимости.")
    @Deprecated(since = "0.1", forRemoval = true)
    @GetMapping("/{accountId}/exist")
    public Boolean isAccountExist(@PathVariable("accountId") Long id) {
        return accountService.isExist(id);
    }

    @Operation(summary = "Получение объекта AccountResponseDto по id аккаунта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Возвращает объект AccountResponseDto", content =
            @Content(schema = @Schema(implementation = AccountResponseDto.class))),
            @ApiResponse(responseCode = "404", description = "Если аккаунт не найден", content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<AccountResponseDto> getAccountById(@PathVariable Long id) {
        return accountService.getAccountDtoById(id).map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
    @Operation(summary = "Проверка существования аккаунта по id и роли")
    @ApiResponse(responseCode = "200", description = "TRUE/FALSE")
    @GetMapping("/{id}/exists")
    public Boolean checkExistByIdAndRole(@PathVariable("id") Long id,
                                         @RequestParam RoleNameEnum role) {
        return accountService.checkExistByIdAndRole(id, role);
    }

}
