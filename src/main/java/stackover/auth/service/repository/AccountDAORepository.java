package stackover.auth.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stackover.auth.service.model.Account;

import java.util.Optional;

@Repository
public interface AccountDAORepository extends JpaRepository<Account, Long> {

    @Query("SELECT acc FROM Account acc WHERE acc.id = :accountId")
    Account findAccountById(Long accountId);

    @Query("SELECT acc FROM Account acc WHERE acc.email = :email")
    Optional<Account> findByEmail(String email);

}
