package stackover.auth.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.auth.service.model.RefreshToken;

import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    @Query("SELECT r FROM RefreshToken r WHERE r.token = :refreshToken")
    Optional<RefreshToken> findByToken(@Param("refreshToken") String refreshToken);

    @Query("SELECT r FROM RefreshToken r WHERE r.account.id = :accountId")
    Optional<RefreshToken> findRefreshTokenByAccountId(@Param("accountId") Long accountId);

    @Query("SELECT r FROM RefreshToken r WHERE r.account.id = :accountId")
    RefreshToken findByAccountId(Long accountId);

    @Query("SELECT r FROM RefreshToken r WHERE r.id = :id")
    Optional<RefreshToken> findRefreshTokenById(@Param("id") Long id);

    @Query("SELECT r FROM RefreshToken r WHERE r.account.email = :subject")
    Optional<RefreshToken> findByEmail(String subject);

    @Modifying
    @Query("DELETE FROM RefreshToken r WHERE r.account.id = :id")
    void deleteByAccountId(Long id);
}
