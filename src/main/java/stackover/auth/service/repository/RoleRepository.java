package stackover.auth.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stackover.auth.service.model.Role;
import stackover.auth.service.util.enums.RoleNameEnum;

@Repository
public interface RoleRepository extends JpaRepository <Role, Long> {

    Role findRoleByName(RoleNameEnum name);
}
