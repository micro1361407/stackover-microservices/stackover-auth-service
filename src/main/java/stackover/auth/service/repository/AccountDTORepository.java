package stackover.auth.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import stackover.auth.service.dto.account.response.AccountResponseDto;
import stackover.auth.service.model.Account;
import stackover.auth.service.util.enums.RoleNameEnum;

import java.util.Optional;

@Repository
public interface AccountDTORepository extends JpaRepository<Account, Long> {

    @Query(value = """
            SELECT NEW stackover.auth.service.dto.account.response.AccountResponseDto(
                    a.id,
                    a.email,
                    a.role.name,
                    a.enabled)
                    FROM Account a WHERE a.id = :accountId
            """
    )
    Optional<AccountResponseDto> findByAccountId(@Param("accountId") Long accountId);

    @Query("SELECT a.id FROM Account a WHERE a.id = :accountId ")
    Optional<Long> isExist(@Param("accountId") Long accountId);

    @Query("SELECT a FROM Account a WHERE a.id = :accountId AND a.role.name = :role")
    Optional<Account> existsAccountsByIdAndRole(@Param("accountId") Long id, @Param("role") RoleNameEnum role);

    @Query("""
            SELECT NEW stackover.auth.service.dto.account.response.AccountResponseDto(
                a.id,
                a.email,
                a.role.name,
                a.enabled)
            FROM Account a WHERE a.email = :email
            """)
    Optional<AccountResponseDto> findByEmail(@Param("email") String email);

    @Query("""
            SELECT NEW stackover.auth.service.dto.account.response.AccountResponseDto(
                a.id,
                a.email,
                a.role.name,
                a.enabled)
                FROM Account a WHERE a.email = :email AND a.password = :password
            """)
    Optional<AccountResponseDto> findByEmailAndPassword(@Param("email") String email, @Param("password") String password);
}
